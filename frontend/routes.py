from flask import Blueprint
from flask_restful import Api
from controller.About import About
from controller.Homepage import HomepageDetails, HomepageDetailsReturnAll
from controller.Status import StatusIDDetails, StatusFileDetails, StatusFileDetailReturnAll, StatusReturnTimeInterval, StatusReturnDate
from controller.Deleteid import DeleteRequestedID
from controller.Mediadetail import MediaDetail

class BluePrintRoutes:
	blueprint_api = Blueprint('api', __name__)

	sample_error = {
    'Case1': {
        'message': "Sample 1 nort found",
        'status': 404,
    },
    'Case2': {
        'message': "ID is not available",
        'status': 406,
    	},
	}

	api = Api(blueprint_api, errors=sample_error)

	#route for about
	api.add_resource(About, '/about')

	#route for homepages
	api.add_resource(HomepageDetails, '/home', '/')
	api.add_resource(HomepageDetailsReturnAll, '/home/all')

	#route for status
	api.add_resource(StatusIDDetails, '/status/id/<string:item_id>', '/status/<string:item_id>')
	api.add_resource(StatusFileDetails, '/status/filename/<string:filename>')
	api.add_resource(StatusFileDetailReturnAll, '/status/filename/all/<string:filename>')
	api.add_resource(StatusReturnDate, '/status/date/<string:entry_date>')
	api.add_resource(StatusReturnTimeInterval, '/status/date/<string:start_date>/to/<string:end_date>')

	#route for delete requests
	api.add_resource(DeleteRequestedID, '/delete/id/<string:item_id>')

	#route for retriving media details alone
	api.add_resource(MediaDetail, '/details/id/<string:item_id>')

