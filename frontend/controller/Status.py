from flask import request
from flask_restful import Resource
from models.LogssModel import db, Logsspage, LogsspageSchema

statuss_schema = LogsspageSchema()
statuss_schemas = LogsspageSchema(many=True)

#Returns the data for a specific Item_ID
class StatusIDDetails(Resource):
	def get(self, item_id):

		statusid = Logsspage.query.get(item_id)
		
		if statusid:
			statusid = statuss_schema.dump(statusid).data
			return {"status":"success", "data":statusid}, 200

		else:
			return {"status":"failed", "message":" ID empty/not available"}, 404

#Returns the datas for the filename
class StatusFileDetailReturnAll(Resource):
	def post(self, filename):

		statusfilename = Logsspage.query.filter_by(filename=filename).all()

		if statusfilename:
			statusfilename = statuss_schemas.dump(statusfilename).data
			return {"status":"success", "data":statusfilename}, 200

		else:
			return {"status":"failed", "message":"empty/not available"}, 401


class StatusFileDetails(Resource):
	def post(self, filename):

		statusfilename = Logsspage.query.filter(Logsspage.filename == filename).all()

		data = [{'Item ID': json_data.item_id, 
			'File Name': json_data.filename, 
			'Status': json_data.overall_job_status,
			'Time Taken': json_data.overall_job_update_time} for json_data in statusfilename]
		
		if statusfilename:
			return {"status":"success", "data":data}, 200
			
		else:
			return {"status":"failed", "message":"empty/not available"}, 401

class StatusReturnTimeInterval(Resource):
	def post(self, start_date, end_date):

		status_from_sd_to_ed = Logsspage.query.filter(Logsspage.entry_date.between(start_date, end_date))

		if not status_from_sd_to_ed:
			return { 'status': 'failed', 'message': 'Empty record'}, 401

		else:
			data = [{'Item ID': json_data.item_id, 
			'File Name': json_data.filename, 
			'Status': json_data.overall_job_status,
			'Time Taken': json_data.overall_job_update_time} for json_data in status_from_sd_to_ed]

			return { 'status': 'success', 'data': data }, 200

class StatusReturnDate(Resource):
	def post(self, entry_date):

		status_entrydate = Logsspage.query.filter(Logsspage.entry_date.like(entry_date+'%')).all()

		if not status_entrydate:
			return { 'status': 'failed', 'message': 'No records found for the date ' + entry_date }

		else:

			data = [{'Item ID': json_data.item_id, 
			'File Name': json_data.filename, 
			'Status': json_data.overall_job_status,
			'Time Taken': json_data.overall_job_update_time} for json_data in status_entrydate]

			return { 'status': 'success', 'data': data }, 200


