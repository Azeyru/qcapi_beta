from flask import jsonify, request
from flask_restful import Resource
from models.LogssModel import db, Logsspage, LogsspageSchema

media_schema = LogsspageSchema()

class MediaDetail(Resource):
	def get(self, item_id):

		media_details = Logsspage.query.get(item_id)

		if not media_details:
			return { 'status': 'error', 'message': 'Records for the ItemID ' +item_id+ ' is not found' }, 404

		else:
			data = {
			'Item ID': media_details.item_id,
			'File Name': media_details.filename,
			'Video Codec': media_details.video_codec_name,
			'Audio Codec': media_details.Audio_codec_name,
			'Audio Sample Rate': media_details.Audio_sample_rate,
			'Audio Channel': media_details.Audio_channel_layout 
			}

			return { 'status': 'success', 'Data': data }, 201
