from flask_restful import Resource

class About(Resource):
	def get(self):
		data = {
                "Name" : "Video Analyzer",
                "Version" : "2.1w",
                "APP" : "Docker Test",
                "Dev" : "Azeyru"
        }
		return data, 200

	def post(self):
		data = {
                "Name" : "Video Analyzer",
                "Version" : "2.1w",
                "APP" : "Docker Test",
                "Dev" : "Azeyru"
        }
		return data, 200