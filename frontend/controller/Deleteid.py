from flask import jsonify, request
from flask_restful import Resource
from models.LogssModel import db, Logsspage, LogsspageSchema

homepages_schema = LogsspageSchema()

#Delete the record for the received id
class DeleteRequestedID(Resource):
	def post(self, item_id):
		delete_request_id = Logsspage.query.get(item_id)

		if not delete_request_id:
			return { 'status':'Error', 'message': 'Item ID not available'}, 404

		else:
			db.session.delete(delete_request_id)
			db.session.commit()

			deleted_data = homepages_schema.dump(delete_request_id).data

			return { 'staus': 'success', 'message': 'Item Deleted', 'Data': deleted_data}, 200
