from flask import jsonify, request
from flask_restful import Resource
from models.LogssModel import db, Logsspage, LogsspageSchema

homepages_schema = LogsspageSchema(many=True)

#Returns all the data from the Data base
#Used for Front page for the Web-App
class HomepageDetailsReturnAll(Resource):
    def get(self):
        Homepages = Logsspage.query.all()
        
        if Homepages:
            Homepages = homepages_schema.dump(Homepages).data
            return { "status":"success", "data":Homepages}, 200
            
        else:
        	return { "status": "NULL", "message": "No entries found in the db" }, 404

#Returns all the data from the Data base, returns filtered JSON
class HomepageDetails(Resource):
    def get(self):
        info = Logsspage.query.all()
            
        if info:
            data = [{'Item ID': json_data.item_id, 
            'File Name': json_data.filename, 
            'Status': json_data.overall_job_status,
            'Time Taken': json_data.overall_job_update_time} for json_data in info]
        
            return { "status":"success", "data":data}, 200

        else:
        	return { "status": "NULL", "message": "No entries found in the db" }, 404
