from flask import Flask
from routes import BluePrintRoutes
from models.LogssModel import db

def load(db_data):
	app = Flask(__name__)
	app.config.from_object(db_data)
	app.register_blueprint(BluePrintRoutes.blueprint_api, url_prefix='/api_qct')
	db.init_app(app)
	return app

if __name__ == "__main__":
    app = load("config")#loads the config.py
    app.run(host="localhost",port=5000,debug=True)#Starts at localhost:5000