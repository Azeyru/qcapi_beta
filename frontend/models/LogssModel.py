from marshmallow import Schema, fields
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy.dialects.mysql import TEXT
from sqlalchemy import String

marsh_obj = Marshmallow()
db = SQLAlchemy()

class Logsspage(db.Model):
    __tablename__ = 'logss'
    item_id = db.Column('item_id',db.Integer, primary_key=True)
    overall_job_status = db.Column('overall_job_status',db.String(20))
    overall_job_update_time = db.Column('overall_job_update_time',db.String(30))
    filename = db.Column('filename',db.TEXT)
    filepath = db.Column('filepath',db.TEXT)
    filesize = db.Column('filesize',db.TEXT)
    fileduration = db.Column('fileduration',db.String(15))
    file_extension = db.Column('file_extension',db.String(10))
    video_codec_name = db.Column('video_codec_name',db.String(10))
    Audio_codec_name = db.Column('Audio_codec_name',db.String(10))
    Audio_sample_rate = db.Column('Audio_sample_rate',db.String(15))
    Audio_channel_layout = db.Column('Audio_channel_layout',db.String(10))
    entry_date = db.Column('entry_date',db.TIMESTAMP)

class LogsspageSchema(marsh_obj.Schema):
    item_id = fields.Integer(required=True)
    overall_job_status = fields.String()
    overall_job_update_time = fields.String()
    filename = fields.Str()
    filepath = fields.Str()
    filesize = fields.Str()
    fileduration = fields.String()
    file_extension = fields.String()
    video_codec_name = fields.String()
    Audio_codec_name = fields.String()
    Audio_sample_rate = fields.String()
    Audio_channel_layout = fields.String()
    entry_date = fields.DateTime()