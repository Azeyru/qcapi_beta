import glob
import os
import os.path
import datetime

class UTIL:
	
	def __init__(self):
		self.messagetxt = "messager_id.txt"
		self.message_folder_ = "processor\\messages_"
	
	#gets the full path for all the media files
	def fullPaths(self, directory):
		datalist = []
		for dirpath,_,filenames in os.walk(directory):
			for files in filenames:
				datalist.append(os.path.abspath(os.path.join(dirpath, files)))
		return datalist
		
	#returns the list of all the video files from the specified text files
	def read_data(self, text_data):
		print("collecting list of all the media files")
		pathlist = []
		for txts in text_data:
			with open(txts, 'r') as pathdata:
				for data in pathdata.readlines():
					if data  is not '\n':
						pathlist.append(str(data).strip())
		#Holds absolutepath for all the video
		video_path_list = []
		for files in pathlist:
			ut = UTIL()
			video_path_list.append(ut.fullPaths(files))

		return [item for sublist in video_path_list for item in sublist]

	#getting ID from text file logged by infoextractor
	def get_ids(self):
		print("Reading the IDs from messager_")
		id_list = []
		with open(os.path.join(self.message_folder_, self.messagetxt)) as ids:
			id = ids.readline()
			while id:
				id_list.append(id.strip())
				id = ids.readline()

		return id_list

class DBASE:
	#fetch the entry date from DB
	def get_entrydate(self, idi):
		from db.logg_queries import QUERIES
		loggQ = QUERIES()
		return loggQ.get_entry_date(idi)

	#updaing the initiation of FFMPEG process
	def update_initial_state(self, idi, status):
		from db.logg_queries import QUERIES
		loggQ = QUERIES()
		loggQ.update_initial_state(idi, status)

	#Updating the complete process 
	def update_state(self, idi, status, timetaken):
		from db.logg_queries import QUERIES
		loggQ = QUERIES()
		loggQ.update_state(idi, status, timetaken)