import pymysql
import pymysql.cursors
import datetime
class QUERIES:
	#defining Variables constructor
	def __init__(self):
		self.host_name = "localhost"
		self.user_name = "root"
		self.password = "root"
		self.db_name = "videologs"

	#opening Connection
	def connections(self):
		print("Opening Connection")
		try:
			connection1 = pymysql.connect(
										host = self.host_name,
										user = self.user_name,
										password = self.password,
										db = self.db_name,
										charset='utf8mb4',
										cursorclass=pymysql.cursors.DictCursor
										)
		except Exception as ex:
			print(ex)
			
		return connection1
		
	#Filewatch 1-insert info to DB
	def insert_fw1(self, EntryTime, Status, currentDT):
		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			idi = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
			self.cursor.execute('INSERT INTO logss(item_id, entry_date, overall_job_status, overall_job_update_time) VALUES('+"'"+str(idi)+"'"+','+"'"+str(EntryTime)+"'"+','+"'"+str(Status)+"'"+','+"'"+str(currentDT)+"'"')')
			connection1.commit()
			print("Logg Table Updated with Filewatcher 1 Info......")
		except Exception as ex:
			print("Failed: Rollbacking..." + ex)
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()

	#Info Extractor 1-insert info to DB
	def insert_info_ext(self, idi, overallstatus, log_name, videos, file_size, extension, entry_time):
	
		'''print("ID: "+ idi)
		print("overallstatus: "+ overallstatus)
		print("log_name: "+ log_name)
		print("videos: "+ videos)
		print("file_size: "+ str(file_size))
		print("extension: "+ extension)
		print("entry_time: "+ str(entry_time))'''

		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			self.cursor.execute('INSERT INTO logss(item_id, overall_job_status, filename, filepath, filesize, file_extension, entry_date) VALUES('
				+"'"+str(idi)+"'"+','
				+"'"+str(overallstatus)+"'"+','
				+"'"+str(log_name)+"'"+','
				+"'"+str(videos)+"'"+','
				+"'"+str(file_size)+"'"+','
				+"'"+str(extension)+"'"+','
				+"'"+str(entry_time)+"'"')')	
			connection1.commit()
			print("logss Table Modified......")
		except Exception as ex:
			print("Failed to Insert data: Rollbacking..." + ex)
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()

	#Update the current state of processess
	def update_initial_state(self, ids, status):
		print("Updating status to logss")

		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			print("Updating State")

			self.cursor.execute("""
				UPDATE logss
				SET overall_job_status=%s
  				WHERE item_id=%s
				""", (status, ids))
			connection1.commit()
			print("Succesfully Updated State...")
		except Exception as ex:
			print("Failed to update State to logss: Rollbacking..." + ex)
			connection1.rollback()
		finally:
			print("Closing connection...")
			connection1.close()

	#Update the current state of processess
	def update_state(self, ids, status, timetaken):
		print("Updating status and time to logss")
		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			print("Updating State")

			self.cursor.execute("""
				UPDATE logss
				SET overall_job_status=%s,
  					overall_job_update_time=%s
  				WHERE item_id=%s
				""", (status, timetaken, ids))
			connection1.commit()
			print("Succesfully Updated State and timetaken...")
		except Exception as ex:
			print("Failed to update State to logss: Rollbacking..." + ex)
			connection1.rollback()
		finally:
			print("Closing connection...")
			connection1.close()

	#Update DB from Info Extractor
	def updateinfo(self, idi, status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel):
		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			print("Updaing Metadata and State")

			self.cursor.execute ("""
   					UPDATE logss
  					SET overall_job_status=%s,
  					overall_job_update_time=%s,
  					fileduration=%s,
  					video_codec_name=%s,
  					Audio_codec_name=%s,
  					Audio_sample_rate=%s,
  					Audio_channel_layout=%s
   					WHERE item_id=%s
					""", (status, timetaken, str(duration), str(v_codec), str(a_codec), str(sample_rate), str(a_channel), idi))

			connection1.commit()
			print("Table logss got Updated")
		except Exception as ex:
			print("Failed to update Info to logss: Rollbacking..." + ex)
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()

	#collecting entry date from logss
	def get_entry_date(self, idi):
		time = ''
		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			print("Getting EntryTime from logss")

			self.cursor.execute("""SELECT entry_date FROM logss WHERE item_id=%s""", (str(idi)))
			diff = self.cursor.fetchone()
			print("EntryTime colleected")
		except Exception as ex:
			print("Failed to get EntryTime from loggs: Rollbacking..." + ex)
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()
		return diff
