import pymysql
import pymysql.cursors
import os
import os.path

class Basedata:
	
	#defining Variables constructor
	def __init__(self):
		#connection Variables
		self.host_name = "localhost"
		self.user_name = "root"
		self.password = "root"
		self.db_name = "videologs"
		
		#table names
		self.blacktable = "blackinfo"
		self.logtable = "logss"
		
		#dir variables
		self.current_path = os.path.dirname(os.path.realpath(__file__))
	
	#opening Connection
	def connection(self):
		try:
			self.connection = pymysql.connect(
										host = self.host_name,
										user = self.user_name,
										password = self.password,
										db = self.db_name,
										charset='utf8mb4',
										cursorclass=pymysql.cursors.DictCursor
										)
			self.cursor = self.connection.cursor()
		except Exception as ex:
			print(ex)
	#Create database 
	def creat_db(self):
		try:
			print("Creating DB: " + self.db_name)
			self.cursor.execute('CREATE DATABASE '+ self.db_name)
		except:
			print("Failed to create DB")
		
	#creat Tables
	'''def creat_tables(self, dtable):
		try:
			#self.cursor = self.connection.cursor()
			print("Creating Table: "+ dtable)
			with open(os.path.join(self.current_path, 'SQL/'+dtable+'.sql')) as black_table_data:
				sqlData = black_table_data.read()
				print(sqlData)
				self.cursor.execute(sqlData)
				print("Success")
		except:
			print("Failed to create the Table: "+dtable)'''
	
	#Checking for Tables in
	def check_tables(self):
		try:
			print("Checking for tables...")
			#self.cursor = self.connection.cursor()
			table = self.cursor.execute('SHOW TABLES FROM '+self.db_name)

			dataz = []
			for tables in self.cursor:
				dataz.append(str(tables))
				
			crt1 = Basedata()
			if not '{'+ "'Tables_in_videologs': "+"'"+ self.blacktable+"'"+'}' in dataz:
				#crt1.creat_tables(self.blacktable)
				print("Creating Table: "+ self.blacktable)
				
				try:
					with open(os.path.join(self.current_path, 'SQL/'+self.blacktable+'.sql')) as black_table_data:
						sqlData = black_table_data.read()
						self.cursor.execute(sqlData)
						print("Successfully created the table "+self.blacktable)
				except Exception as ex:
					print(ex)
					
			else:
				print("The table "+self.blacktable+" already exist")

				
			if not '{'+ "'Tables_in_videologs': "+"'"+ self.logtable+"'"+'}' in dataz:
				#crt1.creat_tables(self.logtable)
				print("Creating Table: "+ self.logtable)
				
				try:
					with open(os.path.join(self.current_path, 'SQL/'+self.logtable+'.sql')) as logg_table_data:
						sqlData = logg_table_data.read()
						self.cursor.execute(sqlData)
						print("Successfully created the table "+self.logtable)
				except Exception as ex:
					print(ex)
					
			else:
				print("The table "+self.logtable+" already exist")

		except Exception as ex:
			print("Unable to show Tables "+ex)
		finally:
			self.connection.close()
	
	#Checking whethere DB exists
	def check_db(self):
		try:
			print("Checking for DB.....")
			#self.cursor = self.connection.cursor()
			DBs = self.cursor.execute("SHOW DATABASES")

			dataz = []
			for key in self.cursor:
				dataz.append(str(key))
	
			if not '{'+"'Database': "+"'"+self.db_name+"'"+'}' in dataz:
				print("Creating DB...")
				crt = Basedata()
				crt.creat_db()
	
			else:
				print("Success, DB Exist")
		except Exception as ex:
			print("Unable to show DB " + ex)
		'''finally:
			self.connection.close()'''

def main():
	try:
		bd = Basedata()
		bd.connection()
		bd.check_db()
		bd.check_tables()
	except:
		print("failed")
	
if __name__ == "__main__":
	main()