import glob
import os
import os.path
import subprocess
import logging
import itertools as tool
from io import StringIO
import datetime
import time

from config import log_source, video_source, time_config, trimmed_video_folder

class TRIMMER():

	def __init__(self):
		self.messagetxt = "messager_id.txt"
		self.message_folder_ = "processor\\messages_"

	#Adding time to actual start and end
	def get_video_details(self, video, start_time, end_time):

		buffer_start_time = str(float(start_time) - float(time_config))
		end_tm = str(float(end_time) + float(time_config))
		duration = str(float(end_tm) - float(buffer_start_time))
	
		output_video_folder = os.path.join(trimmed_video_folder, video)
		if not os.path.exists(output_video_folder):
			os.makedirs(output_video_folder)
			print("Folder Created at:" + output_video_folder)
		
		return buffer_start_time, duration, output_video_folder

	#FFMPEG video processor
	def video_trimmer(self, video_name_path, video, start_time, end_time, count):
		trm = TRIMMER()
		buffer_start_time, duration, output_video_folder = trm.get_video_details(video, start_time, end_time, id)

		print("FFMPEG Initialized...")

		try:
			cmd_trim_video = [
				'C:\\AXIOM\\ffmpeg\\bin\\ffmpeg', 
				'-ss', buffer_start_time, 
				'-i', video_name_path, 
				'-t', duration, 
				'-c:v', 'libx264', '-s', '1280x720', 
				'-b:v', '800k', 
				'-c:a', 'aac', 
				'-b:a', '128k', 
				'-ac', '2', output_video_folder+'\\'+video+str(count)+'.mp4'
				]
			trim_process = subprocess.Popen(
					cmd_trim_video, 
					stdout = subprocess.PIPE, 
					stderr = subprocess.STDOUT)
			cmd_trim_video_data = str(trim_process.communicate())
			print("ffmpeg finished processing the Video")
		
		except Exception as exception:
			print('FFMPEG Threw error at: '+ str(exception))
		
		finally:
			print("Exiting ffmpeg")
		
		return cmd_trim_video_data

	#Getting Start and end time from the corresponding video log folder
	def read_file_data(self, video_path):
	
		video = os.path.basename(video_path).split('.')[0] # contains video name 
		video_log_file = str(os.path.basename(video_path)) + ".log" #location to input videos
		
		print("video: "+video)
		print("video_name_path: "+video_path)
		print("file: "+ video_log_file)
		trm = TRIMMER()
		with open(log_source+'/'+video_log_file,'r') as data:
			count = 0
			for start, end in tool.zip_longest(*[data]*2):

				if end:
					start_time = start.split('=')[1]
					end_time = end.split('=')[1]
					print(video_log_file + ": " + "start time: " + start_time,"end time: " + end_time)
					count += 1
					try:
						trm.video_trimmer(video_path, video, start_time, end_time, count)

					except Exception as err:
						print(err)
						
					finally:
						print('Process Completed....')
	
	def main(self):
		#reading the source localtions from the available text files
		print("Getting Paths from the text file")
		txt_list = ''
		for files in video_source:
			txt_list = glob.glob(os.path.join(files, '*'))

		from util.util import UTIL as ut
		from util.util import DBASE as db
		trm = TRIMMER()
		videos_path = ut.read_data(self, txt_list)#contains list of all video and their paths

		id_list = ut.get_ids(self)
		id_count = 0
		if len(videos_path) == len(id_list):

			for videos in videos_path:
				if os.path.basename(videos) in str(os.listdir(log_source)):
					updated_status = "InProgress/Trimming"

					#Updating the status
					db.update_initial_state(self, id_list[id_count], updated_status)

					#initiating the Trimm process
					trm.read_file_data(videos)

					#getting the process time from logss
					entry_time = db.get_entrydate(self, id_list[id_count])
					for i,datetime1 in entry_time.items():
						pass
					timetaken = datetime.datetime.now() - datetime1
					updated_status = "Complete/VideoTrimm"

					#Updating the complete process to logss
					db.update_state(self, id_list[id_count], updated_status, str(timetaken))

				else:
					print("The log for the video "+os.path.basename(videos)+" is not available in the log folder " +self.messagetxt)
					updated_status = "ErrorProcessing/NoLogsFound"
					db.update_initial_state(self, id_list[id_count], updated_status)
				id_count += 1

			print(" Data has been successfully logged \n")

		elif len(videos_path) > len(id_list):
			print("Log file is missing...")
		
if __name__ == '__main__':
	print("Initiating Video trimmer")
	try:
		trm = TRIMMER()
		trm.main()
	except Exception as err:
		print("Failed to Initiate the process " +err)
	finally:
		print("Exiting video trimmer")