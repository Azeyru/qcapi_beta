import logging
from config import log_destination, info_logs

def load_logger(log_name, extension):

	logger = logging.getLogger('Black')
	logger.setLevel(logging.INFO)
	logger.propagate = False
	
	handler = logging.FileHandler(log_destination+'\\'+log_name+'.'+extension+'.log', mode='w')
	formatter = logging.Formatter('%(asctime)s - %(message)s')
	
	handler.setFormatter(formatter)
	logger.addHandler(handler)
	
	return logger, handler

	
def load_logger_info(log_name, extension):

	logger = logging.getLogger('info')
	logger.setLevel(logging.INFO)
	logger.propagate = False
	
	handler = logging.FileHandler(info_logs+'\\'+log_name+'.'+extension+'.json', mode='w')
	#formatter = logging.Formatter('%(asctime)s - %(message)s')

	#handler.setFormatter(formatter)
	logger.addHandler(handler)
	
	return logger, handler