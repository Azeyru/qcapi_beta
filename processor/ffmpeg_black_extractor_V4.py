import glob
import os
import os.path
import subprocess
import logging
from io import StringIO
import datetime
import time

from config import video_source, log_destination, current_path_movie_input
from settings.log_file import load_logger

log = []
class BLACKINFO:
	#constructor
	def __init__(self):
		#self.log = []
		self.console = logging.getLogger()
		self.messagetxt = "messager_id.txt"
		self.message_folder_ = "processor\\messages_"

	#FFMPEG processor, returns the detected black's frame's time codes
	def cmd_black(self, videos):
		video = videos.replace('\\','/')[2:]
		print("FFMPEG processor started")
		try:
			cmd_black = [
					'C:\\AXIOM\\ffmpeg\\bin\\ffprobe', 
					'-f', 'lavfi', 
					'-i', 'movie='+ video +',blackdetect[out0]', 
					'-show_entries', 
					'tags=lavfi.black_start,lavfi.black_end', 
					'-of', 'default=nw=1', '-v', 'quiet',
					]
			black_process = subprocess.Popen(
				cmd_black, 
				stdout = subprocess.PIPE, 
				stderr = subprocess.STDOUT,
				universal_newlines=True
			)
			for line in (black_process.stdout):
				log.append(line.strip())

			print("data: ........."+ str(log))

		except black_process.CalledProcessError as exception:
			print('Exception occured: ' + str(exception))
		else:
			print('Subprocess Finished, Exiting ffmpeg')
			
		return log
		
	#main method
	def main(self):
		#reading the source localtions from the available text files
		print("Getting Paths from the text file")
		txt_list = ''
		for files in video_source:
			txt_list = glob.glob(os.path.join(files, '*'))
		binf = BLACKINFO()
		from util.util import UTIL as ut
		from util.util import DBASE as db
		videos_path = ut.read_data(self, txt_list)

		id_list = ut.get_ids(self)
		id_count = 0
		if len(videos_path) == len(id_list):
			#iterating through all the videos
			for videos in videos_path:
				updated_status = "InProgress/BlackFrameExtractor"

				#updating the current status of Black frame processor
				db.update_initial_state(self, id_list[id_count], updated_status)

				filename = os.path.basename(videos)
				log_name = filename.split(".")[0]
				extension = filename.split(".")[1]
				print("Loaded file: " + log_name + " | Type: " + extension)
			
				#initilizing logger
				console, handler = load_logger(log_name, extension)

				print("Detecting Black Frames")
				black_data = binf.cmd_black(videos)
				
				remove_duplicate = []
				for data in black_data:
					if data not in remove_duplicate:
						remove_duplicate.append(data)
					
				for data in remove_duplicate:
					console.info(data)

				remove_duplicate[:] = []
				log[:] = []
				console.removeHandler(handler)

				#collecting entry date from DB
				entry_time = db.get_entrydate(self, id_list[id_count])
				for i,datetime1 in entry_time.items():
					pass
				timetaken = datetime.datetime.now() - datetime1
				updated_status = "Complete/BlackFrameExtraction"

				#updating the completed status of Black frame processor
				db.update_state(self, id_list[id_count], updated_status, str(timetaken))
				
				id_count += 1
				print(" Data has been successfully logged \n")
				time.sleep(0.5)

		else:
			print("Error Processing: Kindly Delete the messager_id.txt under messager_ folder and restart the process...")

			
if __name__ == '__main__':
	print("Loading.........................")
	binf = BLACKINFO()
	binf.main()
	print("Black Frame Extraction completed")