import os
import os.path
import subprocess
import glob
from io import StringIO
import datetime

from config import trimmed_video, analysis_video_folder, current_path, video_source

class ANALYZER:

	def __init__(self):
		self.messagetxt = "messager_id.txt"
		self.message_folder_ = "processor\\messages_"

	def cmd_analysis(self, video, file_name, folder_name):
		
		videos = video.replace('\\','/')[1:]
		output_video_folder = os.path.join(analysis_video_folder, folder_name)

		if not os.path.exists(output_video_folder):
			os.makedirs(output_video_folder)
			print("Folder Created at:" + output_video_folder)
			
		print("FFMPEG Started...")
		try:
			cmd_analys = [
				'C:\\AXIOM\\ffmpeg\\bin\\ffmpeg', '-f',
				'lavfi', '-i',
				'amovie='+videos+',asplit=3[sv][eb][av];[sv]showvolume=b=4:w=720:h=68[sv-v];[eb]ebur128=video=1:size=720x540:meter=18[eb-v][out1];[av]avectorscope=s=720x540:draw=line:zoom=1.3:rc=40:gc=160:bc=80:rf=1:gf=8:bf=7[av-v];[sv-v][eb-v][av-v]vstack=3[1c];movie='+videos+',split=4[v][wf][wfc][vs];[wf]waveform=m=1:d=0:r=0:c=7[wf-vus];[wf-vus][v]scale2ref=iw:1148-ih[wf-va][sig];[wf-va]setsar=1[wf-v];[wfc]waveform=m=0:d=0:r=0:c=7,scale=610x684,setsar=1[wfc-v];[vs]vectorscope=m=color3:g=color,scale=610x464,setsar=1[vs-v];[sig][wf-v]vstack[2c];[wfc-v][vs-v]vstack[3c];[1c][2c][3c]hstack=3,scale=1280:-1[out0]', 
				'-async', '1', output_video_folder+'/'+file_name+'.mp4'
				]
			
			analysis_process = subprocess.Popen(cmd_analys, stdout = subprocess.PIPE, stderr = subprocess.STDOUT, universal_newlines = True)
			analysis_process_data = str(analysis_process.communicate())
			print("FFMEPG process finished succesfully.....")
			
		except Exception as exception:
			print('FFMEPG threw: ' + str(exception))
			
		finally:
			print("Exiting FFMEPG...")
						
	def main(self):

		#reading the source localtions from the available text files
		print("Getting Paths from the text file")
		txt_list = ''
		for files in video_source:
			txt_list = glob.glob(os.path.join(files, '*'))
		anz = ANALYZER()
		from util.util import UTIL as ut

		list = []
		for trimmed_folders in os.listdir(trimmed_video):
			list.append(trimmed_folders)

		id_list = ut.get_ids(self)
		id_count = 0

		l_arr = []

		videos_path = ut.read_data(self, txt_list)
		for videos in videos_path:
			video_folder_name = os.path.basename(videos).split(".")[0]
			l_arr.append(video_folder_name)

		for data in l_arr:

			if data in list:
				trimmed_video_list = os.path.join(trimmed_video, data)
				video_names = os.listdir(trimmed_video_list)

				updated_status = "InProgress/Analysis"

				#Updating the status
				from util.util import DBASE as db
				db.update_initial_state(self, id_list[id_count], updated_status)

				for video in video_names:
					current_video_path = os.path.join(current_path, trimmed_video_list, video)

					#initiating FFMPEG processor
					anz.cmd_analysis(current_video_path, video.split(".")[0], os.path.basename(trimmed_video_list))

				entry_time = db.get_entrydate(self, id_list[id_count])
				for i, datetime1 in entry_time.items():
					pass
				timetaken = datetime.datetime.now() - datetime1
				updated_status = "Complete/Analysis"

				#Updating the complete process to logss
				db.update_state(self, id_list[id_count], updated_status, str(timetaken))

			else:
				updated_status = "COMPLETED/CleanFile"

				#Updating the status
				from util.util import DBASE as db
				db.update_initial_state(self, id_list[id_count], updated_status)

			id_count += 1
			print("FFMPEG Process completed...")
			
if __name__ == '__main__':
	print("Initiating Analysis...")
	anz = ANALYZER()
	anz.main()