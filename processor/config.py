import os
import os.path


current_dir_path = os.path.dirname(os.path.realpath(__file__))
current_path = os.getcwd()

video_source = ['video_']
log_destination = "processor\\logs_\\black_info_logs"
info_logs = "processor\\logs_\\media_info_log"
path_to_watch = "video_"
trimmed_video = "processor\\trim_"
trim_analysis = "analysis_"
message_folder = "processor\\messages_"

time_config = 3

current_path_movie_input = current_path.replace('\\','/')[2:]
log_source = current_path + '/' + log_destination

'''Create folders if not available
video_, black_video_ , logs_/black_info_logs'''

video_folder = os.path.join(current_path, path_to_watch)
if not os.path.exists(video_folder):
	os.makedirs(video_folder)
	print("Folder created: " + path_to_watch)

analysis_video_folder = os.path.join(current_path, trim_analysis)
if not os.path.exists(analysis_video_folder):
	os.makedirs(analysis_video_folder)
	print("Folder created: " + trim_analysis)
	
log_destination_folder = os.path.join(current_path, log_destination)
if not os.path.exists(log_destination_folder):
	os.makedirs(log_destination)
	print("Folder created: " + log_destination)
	
media_log_folder = os.path.join(current_path, info_logs)
if not os.path.exists(media_log_folder):
	os.makedirs(info_logs)
	print("Folder created: " + info_logs)

trimmed_video_folder = os.path.join(current_path, trimmed_video)
if not os.path.exists(trimmed_video_folder):
	os.makedirs(trimmed_video_folder)
	print("Folder created: " + trimmed_video)

message_folder_ = os.path.join(current_path, message_folder)
if not os.path.exists(message_folder_):
	os.makedirs(message_folder_)
	print("Folder created: " + message_folder)
