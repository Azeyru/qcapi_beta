import glob
import os
import os.path
import subprocess
import logging
from io import StringIO
import datetime, time
import json

from config import video_source, log_destination, current_path, message_folder_
from settings.log_file import load_logger_info

class INFOEXT:
	#constructor
	def __init__(self):
		self.log = []
		self.console = logging.getLogger()	
		self.data = ""
		self.id = []
		self.messagetxt = "messager_id.txt"
		#self.entry_time = datetime.datetime.now()
		#self.idi = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	
	#FFMPEG processor, returns the extracted Metadata
	def cmd_stream(self, videos):
		print("FFMPEG STARTED...")
		cmd_stream = [
				'C:\\AXIOM\\ffmpeg\\bin\\ffprobe', 
				'-i', videos, '-hide_banner',
				'-print_format','json', 
				#'ffmetadata'
				'-show_format',
				'-show_streams' 
				]
		print("FFMPEG Processing...")
		process_stream = subprocess.Popen(
				cmd_stream, 
				stdout = subprocess.PIPE, 
				stderr = subprocess.STDOUT,
				universal_newlines=True
				)
		for data in (process_stream.stdout):
			self.log.append(data.strip())
		#cmd_stream_data = str(process_stream.communicate())
		return self.log

	#Inserting file infos into DB
	def insertTodb(self, idi, overallstatus, log_name, videos, file_size, extension, entry_time):
		#load the class from db.py
		from db.logg_queries import QUERIES
		loggQ = QUERIES()
		loggQ.insert_info_ext(idi, overallstatus, log_name, videos, file_size, extension, entry_time)

	#Updating db with Metadata
	def updateTodb(self, idi, status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel):
		from db.logg_queries import QUERIES
		loggQ = QUERIES()
		loggQ.updateinfo(idi, status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel)

	#collecting the required metadata from FFMPEG
	def getmeta(self, stream_data):
		index = ''
		indexflag = ''
		duration = ''
		v_codec = ''
		a_codec = ''
		sample_rate = ''
		a_channel = ''

		dictt = []
		for dic in stream_data:
			dic = dic[:-1]
			if dic is not '':
				dictt.append(dic)
		for lines in  range(len(dictt)):
			str_data = dictt[lines]

			#collecting Index number, to get codecs
			if str_data.split(":")[0] == '"index"':
				indexflag = str_data.split(":")[1]

			#collecting video codec based on index 
			if str_data.split(":")[0] == '"codec_name"' and indexflag == ' 0':
				v_codec = str_data.split(":")[1]

			#collecting video codec based on index 
			if str_data.split(":")[0] == '"codec_name"' and indexflag == ' 1':
				a_codec = str_data.split(":")[1]
				
			#collecting Durtation of video
			if str_data.split(":")[0] == '"duration"':
				duration = str_data.split(":")[1]
			#collecting audio sample rate  of video
			if str_data.split(":")[0] == '"sample_rate"':
				sample_rate = str_data.split(":")[1]
			#collecting Audio Channels of video
			if str_data.split(":")[0] == '"channels"':
				a_channel = str_data.split(":")[1]
		return duration, v_codec, a_codec, sample_rate, a_channel

	#Writing the collected ID
	def write_ids(self):
		print("Creating a file for ids...") 
		file_name = "messager_id.txt"
		with open(os.path.join(message_folder_, file_name), 'w') as w_data:
			for i in self.id:
				w_data.write(i+'\n')

	#main method
	def main(self):
		print("Getting Paths from the text file")
		txt_list = ''
		for files in video_source:
			txt_list = glob.glob(os.path.join(files, '*'))
		from util.util import UTIL as ut
		videos_path = ut.read_data(self, txt_list)
		
		if not videos_path:
			print("THE SOURCE FOLDER IS EMPTY.........")
		
		else:
		#iterating through all the videos
			for videos in  videos_path:
				entry_time = datetime.datetime.now()
				idi = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
				updated_status = "In Progress/InfoExtractor"
	
				self.id.append(idi)
	
				filename = os.path.basename(videos)
				log_name = filename.split(".")[0]
				extension = filename.split(".")[1]
				print("Loaded file: " + log_name + " | Type: " + extension)
	
				file_size = round(os.path.getsize(videos)/(1024*1024),5)
	
				#initilizing logger
				console, handler = load_logger_info(log_name, extension)
				arrange = []
				print("Extracting Media Details ")
	
				inf = INFOEXT()
				#inserting into DB
				inf.insertTodb(idi, updated_status, log_name, videos.replace('\\','/'), file_size, extension, entry_time)
	
				stream_data = inf.cmd_stream(videos)
				arrange.append(stream_data)
	
				duration, v_codec, a_codec, sample_rate, a_channel = inf.getmeta(stream_data)
				timetaken = datetime.datetime.now() - entry_time
	
				updated_status = "Completed/InfoExtraction" 
				#Updating DB
				inf.updateTodb(idi, updated_status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel)
	
				for data in arrange:
					console.info(stream_data)
	
				arrange[:] = []
				self.log[:] = []
				console.removeHandler(handler)
				#delay
				time.sleep(1)

if __name__ == '__main__':
	inf = INFOEXT()
	inf.main()	
	inf.write_ids()	
