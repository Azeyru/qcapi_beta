import os
import subprocess

#current_path = os.getcwd()

class DATABASECONFIG:
	#Checking id Table is present
	def main(self):
		try:
			subprocess.call(["python", "processor\\db\\db_settings.py"])

		except Exception as ex:
			print(ex)

class QCTAPI:
	#initiating QCTool's API/Web-App
	def main(self):
		try:
			subprocess.call(["python", "frontend\\run.py"])

		except Exception as ex:
			print(ex)
		
class PROCESSOR:

		def main(self):
			#calling video Metadata extractor
			def info():
				print("Calling FFMPEG for Metadata extraction")
				try:
					subprocess.call(["python", "processor\\ffmpeg_video_info_extractor_V4.py"])
						
				except Exception as ex:
					print(ex)
			info()
			
			#Extracting black parts 
			def b_ext():
				print("Calling FFMPEG for Black parts extraction")
				try:
					subprocess.call(["python", "processor\\ffmpeg_black_extractor_V4.py"])
						
				except Exception as ex:
					print(ex)	
			b_ext()
			
			#trimming the balck parts from videos	
			def trim():	
				print("Calling FFMPEG for trimming black parts extraction")
				try:
					subprocess.call(["python", "processor\\ffmpeg_Trimmer_V3.py"])
						
				except Exception as ex:
					print(ex)
			trim()
			
			#analysisng the trimmed videos	
			def analysis():	
				print("Calling FFMPEG for analysing black parts extraction")
				try:
					subprocess.call(["python", "processor\\ffmpeg_analys_v3.py"])
						
				except Exception as ex:
					print(ex)
			analysis()

class LESSLINES:

	proc_list = [ffmpeg_video_info_extractor_V4.py, ffmpeg_black_extractor_V4.py, ffmpeg_Trimmer_V3.py, ffmpeg_analys_v3.py]
	
	def load_all():
		for ffmpeg_processor in proc_list:
			print("Calling " + ffmpeg_processor)
			try:
				subprocess.call (["python", 'processor\\'+ffmpeg_processor])

			except Exception as ex:
				print(ex)
					
if __name__ == '__main__':
	print("Loading Video QC Tool")
	DBC = DATABASECONFIG()
	DBC.main()

	PROC = PROCESSOR()
	PROC.main()

	#API = QCTAPI()
	#API.main()
	