1. Filewatcher 1
2. Info Extractor
3. Black Extractor
4. Filewatcher 2
5. Trimmer
6. Analysis


1. File watcher:
	Looks whethere ".txt" file has been dropped into "video_" folder
	This will trigger
		a. Info Extractor
		b. Black Extractor

2. Info Extractor
	Open the .txt file from the "video_" folder
	Read the paths in the folder, and loop through all the video files exisiting in the paths, and extracts META DATA from the files.

	DB:
	Insert:
		#File Info
		1. ItemID (idi)
		2. Extry Date (entry_time)
		3. Overall Status (overallstatus)
		4. Overall Job time (get fidd)
		5. FileName (log_name)
		6. File Extension (extension)
		7. File Size (file_size)
		8. File Soruce (videos)
	Update:
		#METADATA
		1. Duration of the video (Duration) ---
		2. Video Codec (index 0, codec_name)
		3. Audio Codec (index 1, codec_name)
		4. Audio Sample rate (sample_rate) ---
		5. Audio Channel (channels)

3. Black Extractor
	Detects the blank space in the videos and logs the time codes. Once the process for all the files are completed this will create a COMPLETE.txt (Empty) inside "messages_"

		DB: 
		Inster: (Will be added later)
			1. FileName
			2. Start Time
			3. End Time
			4. Extry Date
		Update:
			1. Overall Job Time
			2. Overall Status

4. Filewatcher 2
	Looks for COMPELETE.txt and triggers, timmers.py job
		DB:
		Update:
			1. Overall Job Time
			2.Overall Status

5. Trimmer
	Creates Broswe copy for the trimmer files based on the black Info
		DB:
		Update:
			1. Overall Job Time
			2. Overall Status

6. Analyzer
	From The trimmer files, this wil create detalied video logs of the corrupted files
		DB:
		Update:
			1. Overall Job Time
			2. Overall Status



