**Initial release for my VideoAnalysis tool**

1. This is just for black/empty/corrupt portion in a video
2. Requires python 3.5+ and FFMEPG
3. Runs on top of FFMEPG

**Installation**

1. Simply Run **-> "file_watcher_video_V1.py"**
2. Once filewatcher is up, Drop the video into "videos_" folder.
3. You can get the Black video information and Media information from "logs_" folder.
4. Exit FW and Run **-> "ffmpeg_Trimmer_V2.py"**
5. You can get the broswe copy from "trim_" folder
6. To got the complete analysis Run **-> "ffmpeg_analys.py"**
7. Naviagte to "analysis_" to get the videos.

